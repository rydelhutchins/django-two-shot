from django.shortcuts import render
from .models import Receipt, ExpenseCategory, Account

# Create your views here.
def show_receipts(request):
    receipts = Receipt.objects.all()
    context = {
        "receipts": receipts,
    }
    return render(request, "receipts/receipts_list.html", context)

