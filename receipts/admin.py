from django.contrib import admin
from receipts.models import Receipt, ExpenseCategory, Account

# Register your models here.
@admin.register(Receipt)
class AdminReceipt(admin.ModelAdmin):
    list_display = [
        'vendor', 
        'total', 
        'tax', 
        'date', 
        'purchaser', 
        'category', 
        'account',
    ]

@admin.register(ExpenseCategory)
class AdminExpenseCategory(admin.ModelAdmin):
    list_display = [
        'name',
        'owner',
    ]

@admin.register(Account)
class AdminAccount(admin.ModelAdmin):
    list_display = [
        'name', 
        'number', 
        'owner',
    ]